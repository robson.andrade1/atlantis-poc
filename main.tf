variable "region" {
  type    = string
  default = "us-east-1"
}

provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "terraform-state-4tu"
    key    = "atlantis-poc"
    region = "us-east-1"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "atlantis.poc.4tu.app"
  acl    = "private"

  tags = {
    Name        = "atlantis poc"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket" "bucket2" {
  bucket = "atlantis.poc2.4tu.app"
  acl    = "private"

  tags = {
    Name        = "atlantis poc 2"
    Environment = "Dev"
  }
}
